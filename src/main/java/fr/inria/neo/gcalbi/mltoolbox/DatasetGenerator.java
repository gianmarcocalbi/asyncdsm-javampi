package fr.inria.neo.gcalbi.mltoolbox;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.list.NDArrayList;
import fr.inria.neo.gcalbi.utils.Rand;

public class DatasetGenerator {

    public Dataset SyntheticLinearRegression(int m, int p, float errorMean, float errorStdDev){
        INDArray X = Nd4j.zeros(m,p);
        INDArray y = Nd4j.zeros(m);
        INDArray w = Nd4j.ones(p);

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                X.putScalar(i, j, Rand.getInstance().nextGaussian());
            }
        }
        for (int i = 0; i < m; i++) {
            y.put(i, X.getRow(i).mmul(w.transpose()));
        }

        return new Dataset(X, y, w);
    }

    private class Dataset {
        private INDArray X;
        private INDArray y;
        private INDArray w;

        private Dataset(INDArray X, INDArray y, INDArray w) {
            this.X = X;
            this.y = y;
            this.w = w;
        }

        public INDArray getW() {
            return w;
        }

        public void setW(INDArray w) {
            this.w = w;
        }

        public INDArray getY() {
            return y;
        }

        public void setY(INDArray y) {
            this.y = y;
        }

        public INDArray getX() {
            return X;
        }

        public void setX(INDArray x) {
            X = x;
        }
    }
}

