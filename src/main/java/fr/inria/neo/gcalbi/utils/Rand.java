package fr.inria.neo.gcalbi.utils;

import java.util.Random;

// Java program implementing Singleton class
// with getInstance() method
public class Rand extends Random {
    // static variable single_instance of type Singleton
    private static Rand singletonRandomInstance = null;

    // private constructor restricted to this class itself
    private Rand() {
    }

    // static method to create instance of Singleton class
    public static Rand getInstance() {
        if (singletonRandomInstance == null)
            singletonRandomInstance = new Rand();

        return singletonRandomInstance;
    }

    public void setSeed(long seed) {
        super.setSeed(seed);
    }

    public boolean nextBoolean() {
        return super.nextBoolean();
    }

    public double nextDouble() {
        return super.nextDouble();
    }

    public int nextInt(int n) {
        return super.nextInt(n);
    }
}
